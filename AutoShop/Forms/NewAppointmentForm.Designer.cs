﻿namespace AutoShop.Forms
{
    partial class NewAppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewAppointmentForm));
            this.btClose = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.PhoneNoLabel = new System.Windows.Forms.Label();
            this.CarModelLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.ModelTextBox = new System.Windows.Forms.TextBox();
            this.DecriptionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.btCreateAppointment = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.DateTimePick = new System.Windows.Forms.DateTimePicker();
            this.DateLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btClose
            // 
            this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btClose.FlatAppearance.BorderSize = 0;
            this.btClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btClose.ForeColor = System.Drawing.Color.White;
            this.btClose.Image = ((System.Drawing.Image)(resources.GetObject("btClose.Image")));
            this.btClose.Location = new System.Drawing.Point(434, 12);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(45, 35);
            this.btClose.TabIndex = 9;
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.NameLabel.Location = new System.Drawing.Point(41, 106);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(146, 29);
            this.NameLabel.TabIndex = 10;
            this.NameLabel.Text = "Client Name";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.NameTextBox.Location = new System.Drawing.Point(238, 106);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(216, 34);
            this.NameTextBox.TabIndex = 11;
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoEllipsis = true;
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.TitleLabel.Location = new System.Drawing.Point(115, 9);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(261, 36);
            this.TitleLabel.TabIndex = 12;
            this.TitleLabel.Text = "New appointment";
            // 
            // PhoneNoLabel
            // 
            this.PhoneNoLabel.AutoSize = true;
            this.PhoneNoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PhoneNoLabel.Location = new System.Drawing.Point(41, 177);
            this.PhoneNoLabel.Name = "PhoneNoLabel";
            this.PhoneNoLabel.Size = new System.Drawing.Size(127, 29);
            this.PhoneNoLabel.TabIndex = 13;
            this.PhoneNoLabel.Text = "Phone No.";
            // 
            // CarModelLabel
            // 
            this.CarModelLabel.AutoSize = true;
            this.CarModelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.CarModelLabel.Location = new System.Drawing.Point(41, 241);
            this.CarModelLabel.Name = "CarModelLabel";
            this.CarModelLabel.Size = new System.Drawing.Size(131, 29);
            this.CarModelLabel.TabIndex = 14;
            this.CarModelLabel.Text = "Car Model ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label3.Location = new System.Drawing.Point(41, 380);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 29);
            this.label3.TabIndex = 15;
            this.label3.Text = "Description";
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PhoneTextBox.Location = new System.Drawing.Point(238, 177);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(216, 34);
            this.PhoneTextBox.TabIndex = 16;
            // 
            // ModelTextBox
            // 
            this.ModelTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.ModelTextBox.Location = new System.Drawing.Point(238, 241);
            this.ModelTextBox.Name = "ModelTextBox";
            this.ModelTextBox.Size = new System.Drawing.Size(216, 34);
            this.ModelTextBox.TabIndex = 17;
            // 
            // DecriptionRichTextBox
            // 
            this.DecriptionRichTextBox.Location = new System.Drawing.Point(238, 387);
            this.DecriptionRichTextBox.Name = "DecriptionRichTextBox";
            this.DecriptionRichTextBox.Size = new System.Drawing.Size(216, 106);
            this.DecriptionRichTextBox.TabIndex = 19;
            this.DecriptionRichTextBox.Text = "";
            // 
            // btCreateAppointment
            // 
            this.btCreateAppointment.BackColor = System.Drawing.Color.Crimson;
            this.btCreateAppointment.FlatAppearance.BorderSize = 0;
            this.btCreateAppointment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCreateAppointment.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCreateAppointment.ForeColor = System.Drawing.Color.White;
            this.btCreateAppointment.Location = new System.Drawing.Point(46, 531);
            this.btCreateAppointment.Name = "btCreateAppointment";
            this.btCreateAppointment.Size = new System.Drawing.Size(173, 38);
            this.btCreateAppointment.TabIndex = 20;
            this.btCreateAppointment.Text = "Create";
            this.btCreateAppointment.UseVisualStyleBackColor = false;
            this.btCreateAppointment.Click += new System.EventHandler(this.btCreateAppointment_Click);
            // 
            // btCancel
            // 
            this.btCancel.BackColor = System.Drawing.Color.Crimson;
            this.btCancel.FlatAppearance.BorderSize = 0;
            this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCancel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancel.ForeColor = System.Drawing.Color.White;
            this.btCancel.Location = new System.Drawing.Point(281, 531);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(173, 38);
            this.btCancel.TabIndex = 21;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = false;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // DateTimePick
            // 
            this.DateTimePick.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.DateTimePick.Location = new System.Drawing.Point(238, 316);
            this.DateTimePick.Name = "DateTimePick";
            this.DateTimePick.Size = new System.Drawing.Size(216, 21);
            this.DateTimePick.TabIndex = 22;
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.DateLabel.Location = new System.Drawing.Point(41, 316);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(63, 29);
            this.DateLabel.TabIndex = 23;
            this.DateLabel.Text = "Date";
            // 
            // NewAppointmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 598);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.DateTimePick);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btCreateAppointment);
            this.Controls.Add(this.DecriptionRichTextBox);
            this.Controls.Add(this.ModelTextBox);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CarModelLabel);
            this.Controls.Add(this.PhoneNoLabel);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.btClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NewAppointmentForm";
            this.Text = "NewAppointmentForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label PhoneNoLabel;
        private System.Windows.Forms.Label CarModelLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.TextBox ModelTextBox;
        private System.Windows.Forms.RichTextBox DecriptionRichTextBox;
        private System.Windows.Forms.Button btCreateAppointment;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.DateTimePicker DateTimePick;
        private System.Windows.Forms.Label DateLabel;
    }
}