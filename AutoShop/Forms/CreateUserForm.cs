﻿using BAL.Interfaces;
using BAL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoShop.Forms
{
    public partial class CreateUserForm : Form
    {
        private IUserService _userService;
        public CreateUserForm(IUserService userService)
        {
            InitializeComponent();
            _userService = userService;
        }

        private void btAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                int success = InsertUser();
                if (success == 0)
                {
                    MessageBox.Show("User have been inserted succesfully!");
                } else
                {
                    switch (success)
                    {
                        case 1: MessageBox.Show("Invalid username!");
                                break;
                        case 2:
                            MessageBox.Show("Invalid password!");
                            break;
                        case 3:
                            MessageBox.Show("Invalid role!");
                            break;
                    }
                }
                ClearTbs();

            }
            catch(Exception ex)
            {
                MessageBox.Show($"An error has occured. Error message:{ex.Message}");
            }


        }

        private void ClearTbs()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Clear();
                    else
                        func(control.Controls);
            };

            func(Controls);
        }

        private int InsertUser()
        {
            UserModel newUser = new UserModel();
            newUser.FullName = fullNameTb.Text;
            newUser.PhoneNo = phoneTb.Text;
            newUser.Email = emailTb.Text;
            newUser.Username = usernameTb.Text;
            newUser.UserPassword = passwordTb.Text;
            newUser.UserRole = "USER";
            return _userService.AddNewUser(newUser);
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            ClearTbs();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
