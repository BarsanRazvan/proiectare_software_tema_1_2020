﻿using AutoShop.Forms;
using BAL.Interfaces;
using BAL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoShop
{
    public partial class WelcomeForm : Form
    {
        private UserModel _connUser;
        private IUserService _userService;
        BindingSource usersBS = new BindingSource();

        //private bool showAll = false;
        public WelcomeForm(UserModel connectedUserModel, IUserService userService)
        {
            InitializeComponent();
            this._connUser = connectedUserModel;
            this._userService = userService;
            if(_connUser.UserRole.Equals("USER"))
            {
                this.adminToolStripMenuItem.Visible = false;
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        private void createAppoimentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewAppointmentForm appointmentForm = new NewAppointmentForm(_connUser.ID);
            appointmentForm.Show();
        }

        private void appoimentsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AppointmentListForm appointmentList = new AppointmentListForm();
            appointmentList.Show();
        }

        private void userLIstToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var users = _userService.GetUsers().ToList();
            usersBS.DataSource = users;
            UsersForm usersForm = new UsersForm(usersBS);
            usersForm.Show();
        }

        private void addNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateUserForm usersForm = new CreateUserForm(_userService);
            usersForm.Show();
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportsForm reportsForm = new ReportsForm();
            reportsForm.Show();
        }
    }
}
