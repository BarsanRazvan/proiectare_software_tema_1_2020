﻿namespace AutoShop.Forms
{
    partial class AppointmentListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usersDdataGridView = new System.Windows.Forms.DataGridView();
            this.DateTimePick = new System.Windows.Forms.DateTimePicker();
            this.PickDateLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.usersDdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // usersDdataGridView
            // 
            this.usersDdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usersDdataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.usersDdataGridView.Location = new System.Drawing.Point(0, 132);
            this.usersDdataGridView.Name = "usersDdataGridView";
            this.usersDdataGridView.RowHeadersWidth = 51;
            this.usersDdataGridView.RowTemplate.Height = 24;
            this.usersDdataGridView.Size = new System.Drawing.Size(905, 419);
            this.usersDdataGridView.TabIndex = 1;
            // 
            // DateTimePick
            // 
            this.DateTimePick.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.DateTimePick.Location = new System.Drawing.Point(209, 76);
            this.DateTimePick.Name = "DateTimePick";
            this.DateTimePick.Size = new System.Drawing.Size(464, 34);
            this.DateTimePick.TabIndex = 2;
            // 
            // PickDateLabel
            // 
            this.PickDateLabel.AutoEllipsis = true;
            this.PickDateLabel.AutoSize = true;
            this.PickDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.PickDateLabel.Location = new System.Drawing.Point(12, 76);
            this.PickDateLabel.Name = "PickDateLabel";
            this.PickDateLabel.Size = new System.Drawing.Size(159, 36);
            this.PickDateLabel.TabIndex = 13;
            this.PickDateLabel.Text = "Pick Date:";
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(286, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(331, 42);
            this.label1.TabIndex = 14;
            this.label1.Text = "List Appointments";
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Crimson;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(720, 76);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(164, 34);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // AppointmentListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 551);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PickDateLabel);
            this.Controls.Add(this.DateTimePick);
            this.Controls.Add(this.usersDdataGridView);
            this.Name = "AppointmentListForm";
            this.Text = "AppointmentListForm";
            ((System.ComponentModel.ISupportInitialize)(this.usersDdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView usersDdataGridView;
        private System.Windows.Forms.DateTimePicker DateTimePick;
        private System.Windows.Forms.Label PickDateLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
    }
}