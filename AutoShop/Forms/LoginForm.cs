﻿using BAL.Interfaces;
using BAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoShop
{
    public partial class LoginForm : Form
    {
        private bool ValidLogin;
        private IUserService _userService;
        public LoginForm()
        {
            InitializeComponent();
            _userService = new UserService();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            string username = tbUsername.Text;
            string password = tbPassword.Text;
            var connectedUuserModel = _userService.CheckCredentials(username, password);
            if (connectedUuserModel != null)
                ValidLogin = true;
            if (ValidLogin)
            {
                this.Hide();
                WelcomeForm wf = new WelcomeForm(connectedUuserModel, _userService);
                wf.Show();
            } else {
                MessageBox.Show("Invalid Credentials",
                         "Error",
                         MessageBoxButtons.OK);
            }


        }
    }
}
