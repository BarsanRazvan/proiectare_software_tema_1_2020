﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoShop.Forms
{
    public partial class UsersForm : Form
    {
        BindingSource _usersBS;
        public UsersForm(BindingSource usersBS)
        {
            InitializeComponent();
            _usersBS = usersBS;
            InitializeGridView();
        }

        private void InitializeGridView()
        {
            usersDdataGridView.DataSource = _usersBS;
            // Automatically generate the DataGridView columns.
            usersDdataGridView.AutoGenerateColumns = true;
            // Automatically resize the visible rows.
            usersDdataGridView.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            // Set the DataGridView control's border.
            usersDdataGridView.BorderStyle = BorderStyle.Fixed3D;
        }
    }
}
