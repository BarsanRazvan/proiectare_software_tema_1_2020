﻿using BAL.Interfaces;
using BAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoShop.Forms
{
    public partial class ReportsForm : Form
    {
        private BindingSource _appointmentsBS;
        private IAppointmentService _appointmentService;

        public ReportsForm()
        {
            InitializeComponent();
            _appointmentsBS = new BindingSource();
            _appointmentService = new AppointmentService();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            _appointmentsBS.DataSource = _appointmentService.GenerateReports(startTimePicker.Value, endTimePicker.Value).ToList();
            usersDdataGridView.DataSource = _appointmentsBS;
            usersDdataGridView.Refresh();
        }

        private void InitializeGridView()
        {
            usersDdataGridView.DataSource = _appointmentsBS;
            // Automatically generate the DataGridView columns.
            usersDdataGridView.AutoGenerateColumns = true;
            // Automatically resize the visible rows.
            usersDdataGridView.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            // Set the DataGridView control's border.
            usersDdataGridView.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //TO DO:
            MessageBox.Show("To be implemented");
        }
    }
}
