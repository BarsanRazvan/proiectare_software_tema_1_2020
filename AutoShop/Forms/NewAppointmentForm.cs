﻿using BAL.Interfaces;
using BAL.Model;
using BAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoShop.Forms
{
    public partial class NewAppointmentForm : Form
    {
        private int _connectedUserId;
        private IAppointmentService _appointmentService;

        public NewAppointmentForm(int _connectedUserId)
        {
            this._connectedUserId = _connectedUserId;
            _appointmentService = new AppointmentService();
            InitializeComponent();
        }

        private void btCreateAppointment_Click(object sender, EventArgs e)
        {
            AppointmentModel appointment = new AppointmentModel()
            {
                ClientName = NameTextBox.Text,
                ClientPhoneNo = PhoneTextBox.Text,
                CarModel = ModelTextBox.Text,
                ProblemDescription = DecriptionRichTextBox.Text,
                CreationDate = DateTime.Now,
                StatusOfAppointment = "Not Finished",
                CreationUser = _connectedUserId,
                DateOfAppointment = DateTimePick.Value
            };
            _appointmentService.MakeNewAppointment(appointment);
            MessageBox.Show("Appointment Created",
                                     "Success",
                                     MessageBoxButtons.OK);
            this.Dispose();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
