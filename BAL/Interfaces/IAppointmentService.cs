﻿using BAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Interfaces
{
    public interface IAppointmentService
    {
        int MakeNewAppointment(AppointmentModel appointment);
        int FinishAppointment(AppointmentModel appointment);
        AppointmentModel GetAppointmentByDate(DateTime date);
        IEnumerable<AppointmentModel> GenerateReports(DateTime startDate, DateTime endDate);
    }
}
