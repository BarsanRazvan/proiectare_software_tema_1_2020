﻿using BAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Checks if the login credentials are valid
        /// </summary>
        /// <param name="username">Username to be teste</param>
        /// <param name="password">Password to be tested</param>
        /// <returns>An user mapped to the model in case of success, null otherwise</returns>
        UserModel CheckCredentials(String username, String password);
        /// <summary>
        /// Creates a new user with the given credentials and details.
        /// </summary>
        /// <param name="user">User details mapped on the UserModel</param>
        /// <returns>Error Code : 1 Username cannot be null</returns>
        /// <returns>Error Code : 2 Password cannot be null</returns>
        /// <returns>Error Code : 3 User role cannot be null</returns>
        /// <returns>Success Code : 0 User has been created</returns>
        int AddNewUser(UserModel user);

        /// <summary>
        /// Returns all users as an IEnumerable
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserModel> GetUsers();

        /// <summary>
        /// Deletes the selected user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Success Code : 0 User has been created</returns>
        /// <returns>Success Code : 1 User could not be deleted</returns>
        int DeleteUser(UserModel user);
    }
}
