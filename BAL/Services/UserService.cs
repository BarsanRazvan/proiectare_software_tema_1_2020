﻿using AutoMapper;
using BAL.Interfaces;
using BAL.Model;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Services
{
    //TO DO: Exception Handling + dependency injection
    public class UserService : IUserService
    {
        IUnitOfWork unitOfWork;

        public UserService()
        {
            unitOfWork = new UnitOfWork();
        }

        public int AddNewUser(UserModel user)
        {
            if(user.Username == null)
            {
                return 1;
            }

            if (user.UserPassword == null)
            {
                return 2;
            }

            if (user.UserRole == null)
            {
                return 3;
            }
            user.UserPassword = Encrypter.SHA512(user.UserPassword);
            unitOfWork.Users.Insert(ModelMapper.MapValue<UserModel, User>(user));
            unitOfWork.Save();
            return 0;
        }

        public UserModel CheckCredentials(String username, String password)
        {
            password = Encrypter.SHA512(password);
            User currentUser =  unitOfWork.Users.Get(user => user.Username.Equals(username) && password.Equals(user.UserPassword)).FirstOrDefault();
            if(currentUser != null)
            {
                return ModelMapper.MapValue<User, UserModel>(currentUser);
            }

            return null;
        }

        public int DeleteUser(UserModel user)
        {
            //TO DO: another time, another place
            throw new NotImplementedException();
        }

        public IEnumerable<UserModel> GetUsers()
        {
            return unitOfWork.Users.Get(user => !user.UserRole.Equals("INACTIVE")).Select(
                t => ModelMapper.MapValue<User, UserModel>(t)).ToList();
        }
    }
}
