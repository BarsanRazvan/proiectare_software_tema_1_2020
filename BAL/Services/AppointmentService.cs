﻿using AutoMapper;
using BAL.Interfaces;
using BAL.Model;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Services
{
    //TO DO: Exception Handling + dependency injection
    public class AppointmentService : IAppointmentService
    {
        IUnitOfWork unitOfWork;

        public AppointmentService()
        {
            unitOfWork = new UnitOfWork();
        }

        public int FinishAppointment(AppointmentModel appointment)
        {
            if(appointment.ID < 1 || appointment.DateOfAppointment == null)
            {
                return 1;
            }

            appointment.StatusOfAppointment = "Finished";
            unitOfWork.Appointments.Update(ModelMapper.MapValue<AppointmentModel, Appointment>(appointment));
            unitOfWork.Save();
            return 0;
        }

        public IEnumerable<AppointmentModel> GenerateReports(DateTime startDate, DateTime endDate)
        {
            return unitOfWork.Appointments.Get(appointment => DateTime.Compare((DateTime)appointment.DateOfAppointment, startDate) >= 0 &&
                                                            DateTime.Compare((DateTime)appointment.DateOfAppointment, endDate) <= 0).Select(
                                                            t => ModelMapper.MapValue<Appointment, AppointmentModel>(t)).ToList(); 
        }

        public AppointmentModel GetAppointmentByDate(DateTime date)
        {
            Appointment currentAppointment = unitOfWork.Appointments.Get(appointment => DateTime.Compare((DateTime)appointment.DateOfAppointment, date) == 0).FirstOrDefault(); ;
            if (currentAppointment != null)
            {
                return ModelMapper.MapValue<Appointment, AppointmentModel>(currentAppointment);
            }

            return null;
        }

        public int MakeNewAppointment(AppointmentModel appointment)
        {

            unitOfWork.Appointments.Insert(ModelMapper.MapValue<AppointmentModel, Appointment>(appointment));
            unitOfWork.Save();
            return 0;
        }
    }
}
