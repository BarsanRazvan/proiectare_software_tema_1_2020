﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Utilities
{
    class ModelMapper
    {
        private ModelMapper()
        {
        }

        public static V MapValue<T,V>(T valueTobeMapped) where V : class where T : class
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<T, V>();
            });
            IMapper iMapper = config.CreateMapper();
            return iMapper.Map<T, V>(valueTobeMapped);
        }
    }
}
