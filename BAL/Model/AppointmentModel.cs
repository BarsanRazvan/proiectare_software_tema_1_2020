﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Model
{
    public class AppointmentModel
    {
        public int ID { get; set; }
        public DateTime DateOfAppointment { get; set; }
        public string ClientName { get; set; }
        public string ClientPhoneNo { get; set; }
        public string CarModel { get; set; }
        public string ProblemDescription { get; set; }
        public int CreationUser { get; set; }
        public DateTime CreationDate { get; set; }
        public string StatusOfAppointment { get; set; }

        public AppointmentModel()
        {

        }
    }
}
